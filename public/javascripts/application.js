// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

function unshorten(){
    $('.link').each(
	function(index){
	    var rurl = "http://therealurl.appspot.com/?format=json&url=" + $(this).text();
	    $.ajax({
		type: "GET",
		context: this,
		url: rurl,
		data: "{}",
		contentType: "application/json; charset=utf-8",
		dataType: "jsonp",
		success: function(msg) {
		    var title = msg.title != '' ? msg.title : msg.url;
		    $(this).html(title);
		    $(this).attr('href', msg.url);
		},
	    });
	});
}


$(document).ready(function() {
    $('#divtrends').html(loader());
    $.ajax({
	type: "GET",
	//	    context: this,
	url: "http://api.twitter.com/1/trends/weekly.json",
	data: "{}",
	contentType: "application/json; charset=utf-8",
	dataType: "jsonp",
	success: function(msg) {
	    var nb = 0;
	    $('#divtrends').html('<h1>Trends</h1><ul class="trendslist">');
	    $.each(msg.trends, function(){
		$.each(this, function(){
		    if(this.name.substr(0, 1) == '#')
		    {
			$('#divtrends').append("<li><a class='trendLink' href='" + this.url + "'>" + this.name + "</a></li>");
		    }
		});
	    });
	    $('#divtrends').append('</ul>');
	    $('.trendLink').click(function(){
		search($(this).html());
		return false;
	    });

	}
    });

    function loader()
    {
	return '<img src="images/loader.gif" alt="loading"/>';
    }

    function search(txt)
    {
	$('#search').val(txt);
	txt = encodeURIComponent(txt);
	$('.search').html(loader());
	$.ajax({
	    url: '/search/' + txt + '/10', type:'get', dataType:'html',
	    success: function(x){
		$('.search').html(x);
		unshorten();
	    },
	});
    }

    $('#searchForm').submit(function(){

	var val = $('#search').val();
	search(val);
	return false;
    });
});


