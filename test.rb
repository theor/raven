require "./app/helpers/pages_helper.rb"
require 'rubygems'
require 'pp'
require "benchmark"
include PagesHelper

def bench(nb, use_api, compute, extract, print = false)
  t = TweetCrawler.new('link', { :include_linkless => false, :use_api => use_api, :compute => compute})
  t.run nb
  return if !extract
  g = t.extract

  g.each do |group|
    if print then
      p " " * 20 + "[#{group[1].length} #{group[0]}]"
      group[1].each { |x| pp x }
      p group
    else
      group
    end
  end
end

nb = 3
Benchmark.bm(10) do |x|
  x.report("http") do
    bench(nb, false, false, false)
  end
  x.report("json") do
    bench(nb, true,  false, false)
  end



  x.report("http c") do
    bench(nb, false, true, false)
  end
  x.report("json c") do
    bench(nb, true,  true, false)
  end



  x.report("http c e") do
    bench(nb, false, true, true)
  end
  x.report("json c e") do
    bench(nb, true,  true, true)
  end
end

exit
t.tweets.each do |t|
  pp t.links
  pp t.reflink
    end
#  end
#end
