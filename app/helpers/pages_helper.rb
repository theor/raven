module PagesHelper
  require 'rubygems'
  require 'uri'
  require 'net/http'
  require 'nokogiri'
  require 'pp'
  require "curb"

  class Link
    attr_reader :href, :name, :kind

    def initialize(href, kind = nil)
      @href = href
      @name = ''
      if kind == nil then
        extract
      else
        @kind = kind
        @name = href
      end

    end

    def to_s
      "(#{kind.to_s}) #{@name}: #{@href}"
    end

    def extract
      if /\/searches\?q=%23(.*)/ =~ @href then
        @kind = :tag
        @name = Regexp.last_match(1)
      elsif /^\/([^\?\/]*)$/ =~ @href
        @kind = :user
        @name = Regexp.last_match(1)
      else
        @kind = :link
        @name = @href
      end

    end
  end


  class Tweet
    attr_accessor :user, :id, :status, :links, :tags, :users

    def initialize(id, user, status)
      @user = user
      @id = id
      @status = status
      @links = []
      @tags = []
      @users = []
    end

    def link
      "http://twitter.com/#{@user}/status/#{@id}"
    end

    def reflink
      return @links[0].href if @links.length != 0
      nil
    end
  end

  class TweetCrawler
    URL = "http://mobile.twitter.com/searches?"
    @use_api = true
    attr_reader :tweets

    def initialize(tag, options = { })
      @tag = tag
      @tweets = []
      @use_api = options[:use_api] || false
      @include_linkless = options[:include_linkless] || false
      @compute = options[:compute] || true
    end

    def extract
      @tweets.group_by{ |x| x.reflink }.sort{ |a,b| b[1].length <=> a[1].length }
    end

    def run(pageNb = 1)
      for i in 1..pageNb do
        break if !search(@tag, i)
      end
#      @tweets.each do |t|
#        pp t
#        puts
#      end
    end

    def search(tag, page = 1)
      tag =  CGI::escape(tag)
      if @use_api then
        return search_api(tag, page)
      else
        return search_http(tag, page)
      end
    end

    def search_api(tag, page)
      uri = "http://search.twitter.com/search.atom?page=#{page}&q=#{tag}"
      content = get_selector uri
      return if !@compute

      doc = Nokogiri::HTML(content)

      tweets = doc.xpath('//feed/entry');
      return false if tweets.length == 0

      tweets.each do |tweet|
        xid = tweet.xpath('./id').first.content
        id = /:([^:]*)$/.match(xid)[1]

        userurl = tweet.xpath('./author/uri').first.content
        user = /^http:\/\/twitter.com\/([^\/]*)$/.match(userurl)[1]

        status = tweet.xpath('./title').first.content
        tw = Tweet.new(id, user, status)

        if status =~ /@([A-Za-z0-9_]+)/ then
          Regexp.last_match.captures.each do |u|
            tw.users << Link.new(u, :user)
          end
        end

        if status =~ /#([A-Za-z0-9_]+)/ then
          Regexp.last_match.captures.each do |u|
            tw.tags << Link.new(u, :tag)
          end
        end

        if status =~ /((?:ftp|http|https):\/\/(?:\w+:{ 0,1}\w*@)?(?:\S+)(?::[0-9]+)?(?:\/|\/(?:[\w#!:.?+=&%@!\-\/]))?)/ then
          Regexp.last_match.captures.each do |u|
            tw.links << Link.new(u, :link)
          end
        end

        @tweets << tw if @include_linkless || tw.links.length > 0
      end
      return true;
    end

    def search_http(tag, page)
      uri = "#{URL}page=#{page}&q=#{tag}"
      content = get_selector uri
      return if !@compute

      doc = Nokogiri::HTML(content)
      tweets = doc.css('div#tweets-list div.list-tweet')
      return false if tweets.length == 0
      tweets.each do |tweet|
        id = tweet['id'].split('_')[1]
        begin
          user = tweet.xpath('./div/span/strong').first.content

          st = tweet.xpath("./div/span/span")
          status = st.first.content
          tw = Tweet.new(id, user, status)

          st.first.xpath('./a').each do |link|
            clink = Link.new(link['href'])
            case clink.kind
              when :tag
              tw.tags << clink
              when :user
              tw.users << clink
              else
              tw.links << clink
            end
          end
          @tweets << tw if @include_linkless || tw.links.length > 0
        rescue Exception => e
          pp e
          next
        end
      end
      return true
    end

    def get_selector(url)
      get_socket(url)
    end

    def get_curb(uri)
      curl = Curl::Easy.new
      curl.url = uri
      curl.perform
      curl.body_str
    end

    def get_socket(url)
      uri = URI.parse(url)
      # HTTP/1.1 requires handling of chunked transfer-encoding
      tcp_request = <<-HTTP
GET #{ uri.request_uri} HTTP/1.0\r
Host: #{ uri.host}\r
Connection: close\r
\r
HTTP

      s = TCPSocket.open uri.host, uri.port
      s.write tcp_request
      data = s.read
      s.close # hopefully reduces TIME_WAIT duration
      data.split("\r\n\r\n", 2).last # get body
    end

    def get_net(url)
      uri = URI.parse(url)
      Net::HTTP.get uri
    end

  end
end
