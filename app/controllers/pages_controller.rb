class PagesController < ApplicationController
  def index
  end

  def search
    @title = params[:tag]

    nb = 1
    nb = params[:nb].to_i if params.has_key? :nb

    p params

    t = PagesHelper::TweetCrawler.new(params[:tag])
    t.run nb
    @tweets = t.tweets
    @groups = t.extract

    render :layout => false
  end
end
